//
//  TableViewController1.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 31.01.2022.
//

import UIKit

class TableViewController1: UITableViewController {

    @IBAction func addToDo1(_ sender: Any) {
        let alertController = UIAlertController(title: "Create new toDo", message: nil, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "New toDo"
        }
        
        let alertAction1 = UIAlertAction(title: "Cancel", style: .default) { (alert) in
            
        }
        
        let alertAction2 = UIAlertAction(title: "Create", style: .default) { (alert) in
            let newToDo = alertController.textFields![0].text
            HomeWork14.add1(name: newToDo!)
            self.tableView.reloadData()
        }
        
        alertController.addAction(alertAction1)
        alertController.addAction(alertAction2)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeWork14.load1()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
        
        let currentList = todoList[indexPath.row]
        let currentList1 = todoList1[indexPath.row]
        cell.textLabel?.text = currentList.tdlist
        
        if currentList1.tdlist1 == true {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }

        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            HomeWork14.delete1(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        else if editingStyle == .insert{
        
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if HomeWork14.changeState1(at: indexPath.row){
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        } else{
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }

}
