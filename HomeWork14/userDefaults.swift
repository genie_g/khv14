//
//  userDefaults.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 29.10.2021.
//

import Foundation

class userDefaults{
    static let shared = userDefaults()
    
    private let kName = "userDefaults.kName"
    private let kSurname = "userDefaults.kSurname"
    
    var name : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: kName)
        }
        get{
            return UserDefaults.standard.string(forKey: kName)
        }
    }
    
    var surname : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: kSurname)
        }
        get{
            return UserDefaults.standard.string(forKey: kSurname)
        }
    }
}
