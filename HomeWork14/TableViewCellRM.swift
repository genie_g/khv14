//
//  TableViewCellRM.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 11.02.2022.
//

import UIKit

class TableViewCellRM: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var spaciesLabel: UILabel!
    @IBOutlet weak var IV: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
}
