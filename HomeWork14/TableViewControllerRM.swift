//
//  TableViewControllerRM.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 11.02.2022.
//

import UIKit

class TableViewControllerRM: UITableViewController {
    
    var characters: [Results]? = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRM(completion: {character in
            self.characters = character.map { results in

                results.map { getModel($0) }

            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Load().loadCharacters(completion: {characters in
            self.characters = characters
            self.tableView.reloadData()
        } )
        saveRM(characters)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        characters?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! TableViewCellRM
        let ch = characters?[indexPath.row]
        cell.nameLabel.text = ch?.name
        cell.statusLabel.text = ch?.status
        cell.spaciesLabel.text = ch?.species
        cell.locationLabel.text = ch?.location.name
        let url = URL(string: ch?.image ?? "")
            if let data = try? Data(contentsOf: url!)
            {
                cell.IV.image = UIImage(data: data)
            }
        switch ch?.status{
        case ("Alive"): cell.circleLabel.text = "🟢"
        case("Dead"): cell.circleLabel.text = "🔴"
        default: cell.circleLabel.text = "⚪️"
        }
        
        cell.backgroundColor = UIColor.lightGray
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 10
        cell.clipsToBounds = true
        
        return cell
    }

}
