//
//  Model1.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 31.01.2022.
//

import UIKit
import CoreData


var todoList: [TDcoredata] = []
var todoList1: [TDcoredata1] = []


func add1(name: String, isComplited : Bool = false){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let appDelegate1 = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let context1 = appDelegate1.persistentContainer.viewContext
    
    guard let entity = NSEntityDescription.entity(forEntityName: "TDcoredata", in: context) else { return }
    
    guard let entity1 = NSEntityDescription.entity(forEntityName: "TDcoredata1", in: context1) else { return }
    
    let tdObject = TDcoredata(entity: entity, insertInto: context)
    let tdObject1 = TDcoredata1(entity: entity1, insertInto: context1)
    
    tdObject.tdlist = name
    tdObject1.tdlist1 = isComplited
    
    do{
        try context.save()
        try context1.save()
        todoList.append(tdObject)
        todoList1.append(tdObject1)
    } catch let error as NSError{
        print(error.localizedDescription)
    }
}


func delete1(at index : Int){
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let context = appDelegate!.persistentContainer.viewContext
    let fetchRequset: NSFetchRequest<TDcoredata> = NSFetchRequest.init(entityName: "TDcoredata")
    
    let appDelegate1 = UIApplication.shared.delegate as? AppDelegate
    let context1 = appDelegate1!.persistentContainer.viewContext
    let fetchRequset1: NSFetchRequest<TDcoredata1> = NSFetchRequest.init(entityName: "TDcoredata1")
    if let tdcoredata = try? context.fetch(fetchRequset){
        context.delete(tdcoredata[index])
        todoList.remove(at: index)
    }
    if let tdcoredata1 = try? context1.fetch(fetchRequset1){
        context1.delete(tdcoredata1[index])
        todoList1.remove(at: index)
    }
    
    do{
            try context.save()
            try context1.save()
        } catch let error as NSError{
            print(error.localizedDescription)
        }
    
}


func changeState1(at list: Int) -> Bool{
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate!.persistentContainer.viewContext
        let fetchRequset: NSFetchRequest<TDcoredata1> = NSFetchRequest.init(entityName: "TDcoredata1")
    do {
                let tdcoredata = try context.fetch(fetchRequset)[list]
                tdcoredata.tdlist1 = !(tdcoredata.tdlist1)
                do {
                    try context.save()
                } catch {
                    print("ERROR")
                }
            } catch {
                print("ERROR")
            }

    return todoList1[list].tdlist1

}

func load1(){
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let appDelegate1 = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let context1 = appDelegate1.persistentContainer.viewContext
    
    let fetchRequest : NSFetchRequest<TDcoredata> = TDcoredata.fetchRequest()
    let fetchRequest1 : NSFetchRequest<TDcoredata1> = TDcoredata1.fetchRequest()
    
    do{
        todoList = try context.fetch(fetchRequest)
        todoList1 = try context1.fetch(fetchRequest1)
    } catch let error as NSError{
        print(error.localizedDescription)
    }
}



