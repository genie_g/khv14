//
//  saveR&M.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 15.02.2022.
//

import Foundation
import RealmSwift

private let realm = try!Realm()

func saveRM(_ character: [Results]?){
    for el in character!{
        let rrm = ResultsRM()
        rrm.id = el.id
        rrm.name = el.name
        rrm.status = el.status
        rrm.species = el.species
        rrm.type = el.type
        rrm.gender = el.gender
        rrm.origin?.name = el.origin.name
        rrm.origin?.url = el.origin.url
        rrm.location?.name = el.location.name
        rrm.location?.url = el.location.url
        rrm.image = el.image
        try!realm.write{
                realm.add(rrm)
            }
    }
    print(realm.objects(ResultsRM.self))
}

func loadRM(completion: @escaping([ResultsRM]?) -> Void){
    let list = Array(realm.objects(ResultsRM.self))
    completion(list)
}

func getModel(_ dto: ResultsRM) -> Results {
    return Results(id: dto.id, name: dto.name, status: dto.status, species: dto.species, type: dto.type, gender: dto.gender, origin: getOrigin(dto.origin!), location: getLocation(dto.location!), image: dto.image)
}


func getOrigin(_ dto: OriginRM) -> Origin{
    return Origin(name: dto.name, url: dto.url)
}

func getLocation(_ dto: LocationRM) -> Location{
    return Location(name: dto.name, url: dto.url)
}


class ResultsRM: Object, Codable{
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var status: String = ""
    @objc dynamic var species: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var gender: String = ""
    @objc dynamic var origin: OriginRM? = OriginRM()
    @objc dynamic var location: LocationRM? = LocationRM()
    @objc dynamic var image: String = ""
}

class OriginRM: Object, Codable{
    @objc dynamic var name: String? = ""
    @objc dynamic var url: String? = ""
}

class LocationRM: Object, Codable{
    @objc dynamic var name: String = ""
    @objc dynamic var url: String = ""
}
