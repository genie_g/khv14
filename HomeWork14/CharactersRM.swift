//
//  CharactersRM.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 11.02.2022.
//

import Foundation
import RealmSwift


class Characters: Codable{
    var info: Info
    var results: [Results]

    init(info: Info, results: [Results]){
        self.info = info
        self.results = results
    }
}

class Info: Codable{
    var count: Int
    var page: Int?
    var next: String
    var prev: String?

    init(count: Int, page: Int, next: String, prev: String?){
        self.count = count
        self.page = page
        self.next = next
        self.prev = prev
    }
}

class Results: Codable{
    var id: Int
    var name: String
    var status: String
    var species: String
    var type: String
    var gender: String
    var origin: Origin
    var location: Location
    var image: String

    init(id: Int, name: String, status: String, species: String, type: String, gender: String, origin: Origin, location: Location, image: String){
        self.id = id
        self.name = name
        self.status = status
        self.species = species
        self.type = type
        self.gender = gender
        self.origin = origin
        self.location = location
        self.image = image
    }
}

class Origin: Codable{
    var name: String?
    var url: String?

    init(name: String?, url: String?){
        self.name = name
        self.url = url
    }
}

class Location: Codable{
    var name: String
    var url: String

    init(name: String, url: String){
        self.name = name
        self.url = url
    }
}



