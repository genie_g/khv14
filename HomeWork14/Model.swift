//
//  Model.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 24.01.2022.
//

import Foundation
import RealmSwift

var toDoList: [String] = []
var toDoList1: [Bool] = []

private let realm = try!Realm()


func add(name: String, isComplited : Bool = false){
    toDoList.append(name)
    toDoList1.append(isComplited)
    save()
}


func delete(at index : Int){
    toDoList.remove(at: index)
    toDoList1.remove(at: index)
    save()
    
}


func changeState(at list: Int) -> Bool{
    toDoList1[list] = !(toDoList1[list])
    save()

    return toDoList1[list]
}


func save(){
    
    try! realm.write {
      realm.deleteAll()
    }
    
    for el in toDoList{
        let tdr = tdRealm()
        tdr.tdList = el
        try!realm.write{
                realm.add(tdr)
            }
    }
    
    for el in toDoList1{
        let tdr1 = tdRealm1()
        tdr1.tdList1 = el
        try!realm.write{
                realm.add(tdr1)
            }
    }
}

func load(){
    let list = Array(realm.objects(tdRealm.self))
    for el in list{
        toDoList.append(el.tdList)
    }
    
    let list1 = Array(realm.objects(tdRealm1.self))
    for el in list1{
        toDoList1.append(el.tdList1)
    }
}

class tdRealm : Object{
    @objc dynamic var tdList : String = ""
}

class tdRealm1 : Object{
    @objc dynamic var tdList1 : Bool = false
}
