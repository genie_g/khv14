//
//  LoadJSRM.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 11.02.2022.
//

import Foundation
import Alamofire

class Load{
    
    func loadCharacters(completion: @escaping([Results]?) -> Void){
        AF.request("https://rickandmortyapi.com/api/character/?page=1").responseDecodable(of: Characters.self) {response in
            completion(response.value?.results)
        }
    }
 }
