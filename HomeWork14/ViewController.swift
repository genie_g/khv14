//
//  ViewController.swift
//  HomeWork14
//
//  Created by Евгения Головкина on 05.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = userDefaults.shared.name
        surnameTextField.text = userDefaults.shared.surname
        
        nameTextField.addTarget(self, action: #selector(nameTextFieldDidChange(_:)), for: .editingChanged)
        
        surnameTextField.addTarget(self, action: #selector(surnameTextFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func nameTextFieldDidChange(_ textField: UITextField) {
        userDefaults.shared.name = nameTextField.text
    }
    
    @objc func surnameTextFieldDidChange(_ textField: UITextField) {
        userDefaults.shared.surname = surnameTextField.text
    }

}

